#ifndef A_1_13_H_
#define C_2_13_H_

#include "types.h"
#include "header.h"

typedef struct example_thing
{
   int32_t count;
} example_thing;


extern example_thing example_lock ( void );
extern void example_unlock ( example_thing m );

