# MISRA Compliance In GitLab

This is a POC for utilizing CPPcheck to check for MISRA compliance.

**How It Works**

- Uses  [Cppcheck](https://github.com/danmar/cppcheck) with the [misra add-on](https://github.com/danmar/cppcheck/blob/main/addons/misra.py)
- Runs `cppcheck` for all files in the src directory
- Uses [cppcheck-codequality](https://pypi.org/project/cppcheck-codequality/}) to convert `cppcheck` output to the Code Quality report
